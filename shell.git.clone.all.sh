#!/usr/bin/env bash

# Author(s) Peter Pan, Joseph S.

if [ "${SOB_SHELL_GLOBALS}" != "TRUE" ]; then . /c/sob/shell.globals.sh; fi

CLEAN_SLATE="$1"

PLACE="$(pwd)"
BACKUPS="${SOB_}/backups/20$(date +%y%m%d_%s)/"

git_checkout_recurse() {
        dir="$1"; url="$2"; project="$3"; branch="$4"; clean_slate="$5"; name="$6";
        
        repository="${url}${project}";
        
        if [ "${name}" != "" ]; then
                project="${name}";      # If name is set, then that is what we use for the project name
        fi
        
        cd "${SOB}"
        mkdir -p "${dir}"; cd "${dir}";
        
        line 
        log "In directory: $(pwd)"
        line
        
        BREAK="false"
        
        # project exists
        if [ -d "${project}" ]; then
                
                if [ -z "$(ls -A ${project})" ]; then
                        # Is empty, safe to remove
                        rm -rf "${project}"       
                elif [ "${clean_slate}" == "true" ]; then
                        # Not empty, we back it up
                        log "Backing up: ${BACKUPS}${project}";
                        mkdir -p "${BACKUPS}${project}";
                        mv "$project" "${BACKUPS}"                      &> /dev/null
                else
                        log "Project <<< $(pwd)/${project} >>> exists and clean slate flag is not true. Breaking.";  
                        BREAK="true";
                fi
        fi
                        
        if [ "$BREAK" == "false" ]; then
                git clone "${repository}.git" "${project}"
                cd "${project}"                                                 &> /dev/null
                git checkout "${branch}"                                        &> /dev/null 
                git pull                                                        &> /dev/null
                                                                                
                git submodule foreach git reset --hard HEAD                     &> /dev/null
                git submodule foreach git checkout "${branch}"                  &> /dev/null
                git submodule foreach git pull                                  &> /dev/null        
        fi                        
}

{       # Try

        git_checkout_recurse   "$DIR_BACKEND"     "$GIT_URL"    "efsob-dashboard"               "$GIT_BRANCH"           "$CLEAN_SLATE"          "sob-admin"      
        git_checkout_recurse   "$DIR_BACKEND"     "$GIT_URL"    "efsob-catalogue"               "$GIT_BRANCH"           "$CLEAN_SLATE"          "sob-catalogue"  
        git_checkout_recurse   "$DIR_BACKEND"     "$GIT_URL"    "efsob-designer"                "$GIT_BRANCH"           "$CLEAN_SLATE"          "sob-designer"   
        git_checkout_recurse   "$DIR_BACKEND"     "$GIT_URL"    "efsob-stodochbehandling"       "$GIT_BRANCH"           "$CLEAN_SLATE"          "sob-stod"       
                                                                                                                                                
        git_checkout_recurse   "$DIR_FRONTEND"    "$GIT_URL"    "sob-designer"                  "$GIT_BRANCH"           "$CLEAN_SLATE"          ""               
        git_checkout_recurse   "$DIR_FRONTEND"    "$GIT_URL"    "sob-ui"                        "$GIT_BRANCH"           "$CLEAN_SLATE"          ""               
        git_checkout_recurse   "$DIR_FRONTEND"    "$GIT_URL"    "sob-vg"                        "$GIT_BRANCH"           "$CLEAN_SLATE"          ""               
        
        git_checkout_recurse   "$DIR_STAGING"     "$GIT_URL"    "sob-docker-local"              "$GIT_BRANCH"           "$CLEAN_SLATE"          ""                   
        git_checkout_recurse   "$DIR_STAGING"     "$GIT_URL"    "sob-helm"                      "$GIT_BRANCH"           "$CLEAN_SLATE"          ""               
        git_checkout_recurse   "$DIR_STAGING"     "$GIT_URL"    "sob-be-base-image"             "$GIT_BRANCH"           "$CLEAN_SLATE"          ""                    
        git_checkout_recurse   "$DIR_STAGING"     "$GIT_URL"    "sob-fe-base-image"             "$GIT_BRANCH"           "$CLEAN_SLATE"          ""
        
} || {  # Catch
        cd "${PLACE}";
}