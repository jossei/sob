#!/usr/bin/env bash

# Author(s) Peter Pan, Joseph S.

if [ "${SOB_SHELL_GLOBALS}" != "TRUE" ]; then . /c/sob/shell.globals.sh; fi

PLACE="$(pwd)"

git_pull() {
        dir="$1"
        
        cd "${dir}" 
        git pull
}

{
        git_pull "$SOB_STOD"
        git_pull "$SOB_ADMI"
        git_pull "$SOB_CATA"
        git_pull "$SOB_DESI"
} || {
        cd "${PLACE}"
}
