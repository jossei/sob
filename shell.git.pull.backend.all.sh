#!/usr/bin/env bash

# Author(s) Peter Pan, Joseph S.

if [ "${SOB_SHELL_GLOBALS}" != "TRUE" ]; then . /c/sob/shell.globals.sh; fi

sob_mvn_intall() {
        dir="$1"
        
        cd "${dir}"; 
        mvn clean install -DskipTests=true -T 1C
}

{
        sob_mvn_intall "$SOB_STOD"
        sob_mvn_intall "$SOB_ADMI"
        sob_mvn_intall "$SOB_CATA"
        sob_mvn_intall "$SOB_DESI"
} || {
        cd "${PLACE}"
}
