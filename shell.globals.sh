#!/usr/bin/env bash

# Author(s) Peter Pan, Joseph S., Daniel Laic

SOB_SHELL_GLOBALS="TRUE"

LINE="=================================================="

SOB="/c/sob/"                                          
SOB_="${SOB}_"

GIT_URL="https://bitbucket.org/invanartjanster/"
GIT_BRANCH="develop"

DIR_BACKEND="backend"
DIR_FRONTEND="frontend"
DIR_STAGING="staging"

SOB_STOD="${SOB}/${DIR_BACKEND}/sob-stod"
SOB_ADMI="${SOB}/${DIR_BACKEND}/sob-admin"
SOB_CATA="${SOB}/${DIR_BACKEND}/sob-catalogue"
SOB_DESI="${SOB}/${DIR_BACKEND}/sob-design"

function log() {
        echo ">> ${1}"
}

function line() {
        echo "${LINE}"
}